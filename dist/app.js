"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const PORT = 3000;
require('dotenv').config();
const yahoo_1 = __importDefault(require("./routes/yahoo"));
const news_1 = __importDefault(require("./routes/news"));
const counter_1 = __importDefault(require("./routes/counter"));
const app = (0, express_1.default)();
app.use(express_1.default.json());
const publicPath = path_1.default.join(__dirname, "..", "public");
app.get("/", function (req, res) {
    res.sendFile(path_1.default.join(publicPath, "index.html"));
});
app.use('/yahoo', yahoo_1.default);
app.use('/news', news_1.default);
app.use('/counter', counter_1.default);
app.use(express_1.default.static('public'));
app.listen(PORT, "0.0.0.0", () => console.log(`Listening on port ${PORT}......`));
//# sourceMappingURL=app.js.map