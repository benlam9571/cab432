"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
const axios_1 = __importDefault(require("axios"));
require('dotenv').config();
router.get('/:symbol', async (req, res) => {
    let result = {};
    const stockSymbolOption = {
        method: 'GET',
        url: 'https://stocksearch.p.rapidapi.com/api/stocks',
        params: {
            query: req.params.symbol,
            size: '1'
        },
        headers: {
            'X-RapidAPI-Key': process.env.API_KEY,
            'X-RapidAPI-Host': 'stocksearch.p.rapidapi.com'
        }
    };
    let stockSymbolResponse;
    var symbol;
    try {
        stockSymbolResponse = await axios_1.default.request(stockSymbolOption);
        symbol = stockSymbolResponse.data[0].symbol;
    }
    catch (error) {
        console.error(error);
        window.alert("Invalid Stock Name");
    }
    const stockInfoOption = {
        method: 'GET',
        url: `https://stocksearch.p.rapidapi.com/api/stocks/${symbol}`,
        headers: {
            'X-RapidAPI-Key': process.env.API_KEY,
            'X-RapidAPI-Host': 'stocksearch.p.rapidapi.com'
        }
    };
    let stockInfoResponse;
    try {
        stockInfoResponse = await axios_1.default.request(stockInfoOption);
        result = {
            ...stockInfoResponse.data,
        };
    }
    catch (error) {
        console.error(error);
        window.alert("Invalid Stock Name");
    }
    const newsOption = {
        method: 'GET',
        url: 'https://real-time-finance-data.p.rapidapi.com/stock-news',
        params: {
            symbol: `${symbol}}:NASDAQ`,
            language: 'en'
        },
        headers: {
            'X-RapidAPI-Key': process.env.API_KEY,
            'X-RapidAPI-Host': 'real-time-finance-data.p.rapidapi.com'
        }
    };
    let newResponse;
    try {
        newResponse = await axios_1.default.request(newsOption);
        if (newResponse.data.data.news === undefined) {
            result["error"] = "Please enter a valid stock name";
        }
        else {
            result["news"] = newResponse.data.data.news;
        }
    }
    catch (error) {
        console.error(error);
    }
    res.json(result);
    res.end();
});
exports.default = router;
//# sourceMappingURL=news.js.map