import axios from 'https://cdn.jsdelivr.net/npm/axios@1.3.5/+esm';

document.getElementById("priceSubmit").onclick = async function () {
    // Get the input values
    let symbol = document.getElementById("priceSearch").value;
    // Send the object to the server

    let response = await fetch(`/yahoo/${symbol}`, {
        method: "GET",
    });

    let json = await response.json();
    if (json.error !== undefined) {
        window.alert(json.error);
        return;
    }

    let container = document.getElementById("priceResult");

    container.innerHTML = `The Stock price of ${symbol} is $${json.close} in USD, which is $${json.amount} in AUD`;
};

document.getElementById("infoSubmit").onclick = async function () {
    let symbol = document.getElementById("infoSearch").value;

    let response = await fetch(`/news/${symbol}`, {
        method: "GET",
    });

    let json = await response.json();
    console.log(json);
    if (json.error !== undefined) {
        alert(json.error);
        return;
    }

    let nameEl = document.getElementById("name");
    let symbolEl = document.getElementById("symbol");
    let websiteEl = document.getElementById("website");
    let industryEl = document.getElementById("industry");
    let bioEl = document.getElementById("bio");
    let newsEl = document.getElementById("news");

    nameEl.innerHTML = json.name;
    symbolEl.innerHTML = json.symbol;
    websiteEl.innerHTML = json.website;
    industryEl.innerHTML = json.industry;
    bioEl.innerHTML = json.bio;
    newsEl.innerHTML = getNewsHTML(json.news);

};

function getNewsHTML(news) {
    let html = "";
    for (let i = 0; i < news.length; i++) {
        html += `<div class="news-item">
            <h3>${news[i].article_title}</h3>
            <p>${news[i].post_time_utc}</p>
            <a href="${news[i].article_url}">Read More</a>
        </div>`;
    }
    return html;
}

window.onload = async function () {
    console.log('Winodw onload!');
    const res = await axios.post(
        `${window.location.href}counter/getCounter`
    );
    const count = res.data.data.count + 1;
    refreshCounter(count);
    await axios.post(
        `${window.location.href}counter/updateCounter`,
        {
            count: count,
        }
    );
};

const refreshCounter = function (count) {
    const countDiv = document.getElementById('counter');
    countDiv.innerText = `Number of Visitor: ${count}`;
};