import express from 'express';
import path from 'path';

const PORT = 3000;
require('dotenv').config();

import yahooRoute from './routes/yahoo';
import newsRoute from './routes/news';
import counterRoute from './routes/counter';

const app = express();

app.use(express.json());

const publicPath = path.join(__dirname, "..", "public");

app.get("/", function (req, res) {
    res.sendFile(path.join(publicPath, "index.html"));
});

app.use('/yahoo', yahooRoute);
app.use('/news', newsRoute);
app.use('/counter', counterRoute);
app.use(express.static('public'))


app.listen(PORT, "0.0.0.0", () =>
    console.log(`Listening on port ${PORT}......`)
);
