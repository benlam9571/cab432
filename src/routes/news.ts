import express from 'express';
const router = express.Router();
import axios from 'axios';
require('dotenv').config();


router.get('/:symbol', async (req, res) => {

    let result = {};

    const stockSymbolOption = {
        method: 'GET',
        url: 'https://stocksearch.p.rapidapi.com/api/stocks',
        params: {
            query: req.params.symbol,
            size: '1'
        },
        headers: {
            'X-RapidAPI-Key': process.env.API_KEY,
            'X-RapidAPI-Host': 'stocksearch.p.rapidapi.com'
        }
    };
    let stockSymbolResponse;
    var symbol;

    try {
        stockSymbolResponse = await axios.request(stockSymbolOption);
        symbol = stockSymbolResponse.data[0].symbol;
    } catch (error) {
        console.error(error)
        window.alert("Invalid Stock Name");
    }

    const stockInfoOption = {
        method: 'GET',
        url: `https://stocksearch.p.rapidapi.com/api/stocks/${symbol}`,

        headers: {
            'X-RapidAPI-Key': process.env.API_KEY,
            'X-RapidAPI-Host': 'stocksearch.p.rapidapi.com'
        }
    };
    let stockInfoResponse;

    try {
        stockInfoResponse = await axios.request(stockInfoOption);
        result = {
            ...stockInfoResponse.data,
        };


    } catch (error) {
        console.error(error)
        window.alert("Invalid Stock Name");
    }

    const newsOption = {
        method: 'GET',
        url: 'https://real-time-finance-data.p.rapidapi.com/stock-news',
        params: {
            symbol: `${symbol}}:NASDAQ`,
            language: 'en'
        },
        headers: {
            'X-RapidAPI-Key': process.env.API_KEY,
            'X-RapidAPI-Host': 'real-time-finance-data.p.rapidapi.com'
        }
    };

    let newResponse;

    try {
        newResponse = await axios.request(newsOption);
        if (newResponse.data.data.news === undefined) {
            result["error"] = "Please enter a valid stock name";
        }
        else {
            result["news"] = newResponse.data.data.news;
        }
    } catch (error) {

        console.error(error);
    }

    res.json(result);
    res.end();

});


export default router;
