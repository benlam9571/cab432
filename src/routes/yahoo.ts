import express, { response } from 'express';
const router = express.Router();
import axios from 'axios';
require('dotenv').config();



router.get('/:symbol', async (req, res) => {
    const options = {
        method: 'GET',
        url: 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v3/get-historical-data',
        params: { symbol: req.params.symbol, region: 'US' },
        headers: {
            'X-RapidAPI-Key': process.env.API_KEY,
            'X-RapidAPI-Host': 'apidojo-yahoo-finance-v1.p.rapidapi.com'
        }
    };

    var close;

    try {
        const response = await axios.request(options);
        console.log(response.data.prices[0].close);
        close = response.data.prices[0].close;
    } catch (error) {
        console.log('error')
        res.json({
            error: "Invalid Stock Symbol",
        });
        res.end();
        return;
    }
    if (!close) {
        res.write('The stock price is not available');
        res.end();
    }

    const option2 = {
        method: 'GET',
        url: 'https://currency-converter5.p.rapidapi.com/currency/convert',
        params: {
            format: 'json',
            from: 'USD',
            to: 'AUD',
            amount: close
        },
        headers: {
            'X-RapidAPI-Key': process.env.API_KEY,
            'X-RapidAPI-Host': 'currency-converter5.p.rapidapi.com'
        }
    };
    var amount;

    try {
        const response = await axios.request(option2);
        amount = response.data.rates.AUD.rate_for_amount;
    }
    catch (error) {
        console.error(error);
    }
    console.log(amount);
    res.json({
        close: close,
        amount: amount,
    });
    res.end();
}
);



export default router;

