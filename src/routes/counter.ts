
import AWS from 'aws-sdk';
import express from 'express';
const router = express.Router();
const fs = require('fs');



async function createS3bucket(s3, bucketName) {
    console.log('Creating Bucket!');
    try {
        console.log(bucketName);
        await s3.createBucket({ Bucket: bucketName }).promise();
        console.log(`Created bucket: ${bucketName}`);
        return s3;
    } catch (err) {
        if (err.statusCode === 409) {
            console.log(`Bucket already exists: ${bucketName}`);
        } else {
            console.log(`Error creating bucket: ${err}`);
        }
    }
}

async function uploadJsonToS3(
    s3,
    bucketName,
    objectKey,
    jsonData
) {
    console.log('Uploading Json to S3!');
    const params = {
        Bucket: bucketName,
        Key: objectKey,
        Body: JSON.stringify(jsonData),
        ContentType: 'application/json',
    };

    try {
        await s3.putObject(params).promise();
        console.log('JSON file uploaded successfully.');
    } catch (err) {
        console.error('Error uploading JSON file:', err);
    }
}


async function getObjectFromS3(s3, bucketName, objectKey) {
    console.log('Getting object from S3!');
    const params = {
        Bucket: bucketName,
        Key: objectKey,
    };

    try {
        const data = await s3.getObject(params).promise();
        console.log(data.Body);
        const parsedData = JSON.parse(
            data.Body.toString('utf-8')
        );
        console.log('Parsed JSON data:', parsedData);
        return parsedData;
    } catch (err) {
        console.error('Error:', err);
        return 'Error';
    }
}


router.post('/getCounter', async function (req, res, next) {
    AWS.config.update({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        sessionToken: process.env.AWS_SESSION_TOKEN,
        region: process.env.AWS_BUCKET_REGION,
    });


    const s3 = new AWS.S3();


    const bucketName = process.env.AWS_BUCKET_NAME;
    const objectKey = 'counter.json';

    await createS3bucket(s3, bucketName);
    const rsp = await getObjectFromS3(
        s3,
        bucketName,
        objectKey
    );
    if (rsp === 'Error') {
        return res.status(500).json({ success: false });
    } else {
        return res.status(200).json({
            success: true,
            data: rsp,
        });
    }
});

router.post(
    '/updateCounter',
    async function (req, res, next) {
        const { count } = req.body;
        AWS.config.update({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
            sessionToken: process.env.AWS_SESSION_TOKEN,
            region: process.env.AWS_BUCKET_REGION,
        });


        const s3 = new AWS.S3();


        const bucketName = process.env.AWS_BUCKET_NAME;
        const objectKey = 'counter.json';


        const jsonData = {
            count: count,
        };
        await createS3bucket(s3, bucketName);
        await uploadJsonToS3(
            s3,
            bucketName,
            objectKey,
            jsonData
        );
    }
);

export default router;